
import * as vscode from 'vscode';
import * as fs from 'fs';

export class AppUtils {
  mkdirSyncRecursive(directory: string) {
    let path = directory.replace(/\/$/, '').split('/');
    for (let i = 1; i <= path.length; i++) {
        let segment = path.slice(0, i).join('/');
        segment.length > 0 && !fs.existsSync(segment) ? fs.mkdirSync(segment) : null ;
    }
  }
}