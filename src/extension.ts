import * as vscode from 'vscode';
import * as fs from 'fs';
import * as path from 'path';
import {AppUtils} from "./appUtils";

export function activate(context: vscode.ExtensionContext) {

	let disposable = vscode.commands.registerCommand('extension.genjunit', (selectedFile, selectedFiles) => {

		for (let file of selectedFiles) {
			let newFile = "";
			if (file.path.indexOf("src/main") !== -1) { // maven
				newFile = file.path.replace(/^(.+)\/src\/main/, "$1/src/test").replace(".java", "Test.java");
			} else { // dynamic web project (eclipse)
				newFile = file.path.replace(/^(.+)\/src\//, "$1/test/").replace(".java", "Test.java");
			}

			try {
				console.log('디렉토리생성');
				const appUtils = new AppUtils();
				appUtils.mkdirSyncRecursive(path.dirname(newFile));
				//fs.mkdirSync(path.dirname(newFile), {recursive: true});
			} catch (error) { console.error(error); }

			let newText = [];
			try {
				let text = fs.readFileSync(file.path, 'utf8');
				let textTmp = text.split("\n");

				for (let line of textTmp) {
					if (line.match(/package /)) {
						newText.push(line);
						newText.push("");
						newText.push("import org.junit.Test;");
						newText.push("import org.junit.Before;");
						newText.push("");
					}

					let c = line.match(/.+class ([^ ]+)/);
					if (c) {
						newText.push('public class '+c[1]+'Test {');
						newText.push("");
						newText.push("\t@Before");
						newText.push("\tpublic void setUp() throws Exception {");
						newText.push("");
						newText.push("\t}");
					}

					let f = line.match(/(protected|public).* ([^ ]+)[ ]?\(/);
					if (f) {
						let fn = "test" + f[2].charAt(0).toUpperCase() + f[2].slice(1);
						newText.push("\t@Test");
						newText.push("\tpublic void " + fn + "() throws Exception {");
						newText.push("\t// GIVEN");
						newText.push("\t// WHEN");
						newText.push("\t// THEN");
						newText.push("\t}");
						newText.push("");
					}
				}

				newText.push("}\n");

			} catch (error) { console.error(error); }

			try {
				console.log('파일생성');
				// 기존에 존재하는 파일은 건드리지 않기
				fs.writeFileSync(newFile, newText.join("\n"), {flag:"wx"});
			} catch (error) { console.error(error); }

			vscode.workspace.openTextDocument(newFile).then((editor) => {
				if (!editor) {
					return;
				}
				vscode.window.showTextDocument(editor);
			});
		}

		vscode.commands.executeCommand("workbench.files.action.refreshFilesExplorer");
		vscode.window.showInformationMessage(selectedFiles.length + " files generated!");
	});

	context.subscriptions.push(disposable);
}

export function deactivate() {}
