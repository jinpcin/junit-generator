## Features

Generate junit file using existing file

Support

- Apache Maven Standard Directory Layout

- Eclipse-based Web Application (Dynamic Web Project) Directory Layout ( required adding classpath for "/test" )
>  For example, in the ".classpath" file adding the following

```
<classpathentry kind="src" output="target/test-classes" path="test">
  <attributes>
    <attribute name="test" value="true"/>
  </attributes>
</classpathentry>
 ```

메이븐 디렉토리 구조와 eclipse(Dynamic Web Project) 디렉토리 구조를 지원함
eclipse 디렉토리 구조는 .classpath 에 "/test" 디렉토리에 대한 classpath 를 지정해야 함

## Requirements


## Extension Settings


## Known Issues


## Release Notes

### 0.0.1

필요에 의해서 급히 만듬

I just made it urgently by necessity

### 0.0.2

쓰다보니 다수개를 한꺼번에 생성하는 것도 필요해짐

I need to create a few number of files at once
